mAccel = mSenseManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			mGyro = mSenseManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			mGrav = mSenseManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
			mRotVec = mSenseManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
			mMag = mSenseManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
			mLinAccel = mSenseManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

new Runnable() {
				@Override
				public void run() {
					mSenseManager.registerListener(mListener,mAccel,SensorManager.SENSOR_DELAY_GAME);
					mSenseManager.registerListener(mListener,mGyro,SensorManager.SENSOR_DELAY_GAME);
					mSenseManager.registerListener(mListener,mLinAccel,SensorManager.SENSOR_DELAY_GAME);
					mSenseManager.registerListener(mListener,mGrav,SensorManager.SENSOR_DELAY_GAME);
					mSenseManager.registerListener(mListener,mRotVec,SensorManager.SENSOR_DELAY_GAME);
				}
			}