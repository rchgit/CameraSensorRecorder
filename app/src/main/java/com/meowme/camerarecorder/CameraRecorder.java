/*
 * Copyright (c) 2015, Picker Weng
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of CameraRecorder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project:
 *     CameraRecorder
 *
 * File:
 *     CameraRecorder.java
 *
 * Author:
 *     Picker Weng (pickerweng@gmail.com)
 */

package com.meowme.camerarecorder;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class CameraRecorder extends Activity implements SurfaceHolder.Callback {

    private static final String TAG = CameraRecorder.class.getSimpleName();

    private static SurfaceView mSurfaceView;
    static SurfaceHolder mSurfaceHolder;
    static Camera mCamera;
    private RecorderService mRecorderService;
    private boolean mBounded;
    private final int REQUEST_CAMERA = 1;
    private final int REQUEST_AUDIO = 2;
    private final int REQUEST_EXTERNAL_STORAGE = 3;
    private Button btnStart;
    private Button btnStop;
    private int ALL_PERMISSIONS_GRANTED = 0;

    public CameraRecorder() {
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.main);

        mSurfaceView = (SurfaceView) findViewById(R.id.surfaceView1);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

    }

    @Override
    protected void onStart() {
        super.onStart();

//        if (ContextCompat.checkSelfPermission(CameraRecorder.this,
//                Manifest.permission.CAMERA)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(CameraRecorder.this,
//                    new String[]{Manifest.permission.CAMERA},
//                    REQUEST_CAMERA);
//        }
//        else
//            ALL_PERMISSIONS_GRANTED += 1;
//        if (ContextCompat.checkSelfPermission(CameraRecorder.this,
//                Manifest.permission.RECORD_AUDIO)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(CameraRecorder.this,
//                    new String[]{Manifest.permission.RECORD_AUDIO},
//                    REQUEST_AUDIO);
//        }
//        else
//            ALL_PERMISSIONS_GRANTED += 1;
//        if (ContextCompat.checkSelfPermission(CameraRecorder.this,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(CameraRecorder.this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    REQUEST_EXTERNAL_STORAGE);
//        }
//        else
//            ALL_PERMISSIONS_GRANTED += 1;
        Intent mIntent = new Intent(this, RecorderService.class);
        bindService(mIntent, mConnection, BIND_AUTO_CREATE);
        btnStart = (Button) findViewById(R.id.StartService);
        btnStart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Intent intent = new Intent(CameraRecorder.this, RecorderService.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //startService(intent);
                // Permissions check for Android 6.0+

//                if (ALL_PERMISSIONS_GRANTED == 3)
                    mRecorderService.startRecording();
//                else
//                    Toast.makeText(CameraRecorder.this, "Please grant all permissions to enable recording.",
//                            Toast.LENGTH_LONG).show();

                //moveTaskToBack(true);
                //getApplicationContext().unbindService(mConnection);
                //finish();
            }
        });

        btnStop = (Button) findViewById(R.id.StopService);
        btnStop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG, "Stopped");
                Toast.makeText(getBaseContext(), "Stopped", Toast.LENGTH_LONG).show();
                //stopService(new Intent(CameraRecorder.this, RecorderService.class));
                mRecorderService.stopRecording();
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    ALL_PERMISSIONS_GRANTED += 1;

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    ALL_PERMISSIONS_GRANTED -= 1;
                }

            }
            case REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ALL_PERMISSIONS_GRANTED += 1;

                } else {

                    ALL_PERMISSIONS_GRANTED -= 1;
                }

            }
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ALL_PERMISSIONS_GRANTED += 1;

                } else {

                    ALL_PERMISSIONS_GRANTED -= 1;
                }

            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("CameraRecorder", "I'm paused");
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.d("CameraRecorder", "I'm stopped");

    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceDisconnected(ComponentName name) {
            Toast.makeText(CameraRecorder.this, "Service is disconnected", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Service is disconnected");
            mBounded = false;
//			mRecorderService = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            Toast.makeText(CameraRecorder.this, "Service is connected", Toast.LENGTH_SHORT).show();
            mBounded = true;
            RecorderService.LocalBinder mLocalBinder = (RecorderService.LocalBinder) service;
            mRecorderService = mLocalBinder.getServerInstance();
            Log.d(TAG, "Service is connected");
        }
    };

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    protected void onDestroy() {

//		getApplicationContext().unbindService(mConnection);
        super.onDestroy();
        Log.d("CameraRecorder", "I'm destroyed");

    }

}
