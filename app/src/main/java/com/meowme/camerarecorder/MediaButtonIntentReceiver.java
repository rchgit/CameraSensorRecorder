package com.meowme.camerarecorder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;

import static android.content.ContentValues.TAG;

public class MediaButtonIntentReceiver extends BroadcastReceiver {
    public MediaButtonIntentReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
            final KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

            if (event != null && event.getAction() == KeyEvent.ACTION_DOWN) {
                switch (event.getKeyCode()) {
                    case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                        //context.startService(new Intent(context, RecorderService.class));
                        Log.d(TAG,"Play/Pause Pressed");
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PLAY:
                        Log.d(TAG,"Play Pressed");
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PAUSE:
                        Log.d(TAG,"Paused Pressed");
                        break;
                }
            }
        }
    }
}
