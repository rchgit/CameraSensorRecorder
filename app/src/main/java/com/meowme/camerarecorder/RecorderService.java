/*
 * Copyright (c) 2015, Picker Weng
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of CameraRecorder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Project:
 *     CameraRecorder
 *
 * File:
 *     CameraRecorder.java
 *
 * Author:
 *     Picker Weng (pickerweng@gmail.com)
 */

package com.meowme.camerarecorder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.*;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.widget.Toast;
import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.media.MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED;

//import au.com.bytecode.opencsv.CSVReader;
//import android.support.v4.app.NotificationCompat.Builder;


public class RecorderService extends Service {
    private static final String TAG = "RecorderService";
    private SurfaceHolder mSurfaceHolder;
    private static Camera mServiceCamera;
    private boolean mRecordingStatus;
    private MediaRecorder mMediaRecorder;
    private int mNotificationId = 1;
    private NotificationManager mNotificationManager;
    // Sensors
    private SensorManager mSenseManager;
    private SensorEventListener mListener;
    private Sensor mAccel;
    private Sensor mGyro;
    private Sensor mMag;
    private String mPath;
    private ExecutorService mExecService;

    private MediaSessionCompat mMediaSession;
    private Handler mCsvHandler = new Handler();
    private long mSavePeriod = 100; // in millisec

    private StringBuffer mValBuf = new StringBuffer();


    private float[] mAccelVal = new float[3];
    private float[] mGyroVal = new float[3];
    private float[] mMagVal = new float[3];
    private float[][] mValArray;
    private CSVWriter mSensorFile;
    private Ringtone r;
    private int mRecordDuration = 15 * 1000; // 15 seconds


    private class SensorHandler implements Runnable {

        @Override
        public void run() {
            // Register sensor listeners with sampling frequency for gaming
            mSenseManager.registerListener(mListener, mAccel, SensorManager.SENSOR_DELAY_GAME);
            mSenseManager.registerListener(mListener, mGyro, SensorManager.SENSOR_DELAY_GAME);
            mSenseManager.registerListener(mListener, mMag, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    private IBinder mBinder = new LocalBinder();


    class LocalBinder extends Binder {
        RecorderService getServerInstance() {
            return RecorderService.this;
        }
    }

    private class SensorListener implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case android.hardware.Sensor.TYPE_ACCELEROMETER:
                    mAccelVal[0] = event.values[0];
                    mAccelVal[1] = event.values[1];
                    mAccelVal[2] = event.values[2];
                    mValArray[0] = mAccelVal;
                    //Log.d(TAG, "Acceleration: " + mAccelVal[0] + "," + mAccelVal[1] + "," + mAccelVal[2]);
                    break;
                // COMMENTED OUT AS PER PROPOSAL REVISIONS
//				case Sensor.TYPE_LINEAR_ACCELERATION:
//					mLinAccelVal[0] = event.values[0];
//					mLinAccelVal[1] = event.values[1];
//					mLinAccelVal[2] = event.values[2];
//					mValArray[3] = mLinAccelVal;
//					//Log.d(TAG, "Lin Acceleration: " + mLinAccelVal[0] + ", " + mLinAccelVal[1] + ", " + mLinAccelVal[2]);
//					break;
//				case Sensor.TYPE_GRAVITY:
//					mGravVal[0] = event.values[0];
//					mGravVal[1] = event.values[1];
//					mGravVal[2] = event.values[2];
//					mValArray[1] = mGravVal;
//					//Log.d(TAG, "Gravity: " + mGravVal[0] + ", " + mGravVal[1] + ", " + mGravVal[2]);
//					break;
                case Sensor.TYPE_GYROSCOPE:
                    mGyroVal[0] = event.values[0];
                    mGyroVal[1] = event.values[1];
                    mGyroVal[2] = event.values[2];
                    mValArray[1] = mGyroVal;
                    //Log.d(TAG, "Gyro: " + mGyroVal[0] + ", " + mGyroVal[1] + ", " + mGyroVal[2]);
                    break;
                // COMMENTED OUT AS PER PROPOSAL REVISIONS
//				case Sensor.TYPE_ROTATION_VECTOR:
//					mRotVecVal[0] = event.values[0];
//					mRotVecVal[1] = event.values[1];
//					mRotVecVal[2] = event.values[2];
//					mValArray[5] = mRotVecVal;
//					//mRotVecVal[3] = event.values[3];
//
//					//Log.d(TAG, "Rotation Vector: " + mRotVecVal[0] + ", " + mRotVecVal[1] + ", " + mRotVecVal[2]);
//					break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    mMagVal[0] = event.values[0];
                    mMagVal[1] = event.values[1];
                    mMagVal[2] = event.values[2];
                    mValArray[2] = mMagVal;
                    //Log.d(TAG, "Magnetic Vector: " + mMagVal[0] + ", " + mMagVal[1] + ", " + mMagVal[2]);
                    break;
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            switch (sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    Log.d(TAG, "Accelerometer accuracy changed");
                case Sensor.TYPE_GYROSCOPE:
                    Log.d(TAG, "Gyroscope accuracy changed");
                case Sensor.TYPE_MAGNETIC_FIELD:
                    Log.d(TAG, "Magnetic sensor accuracy changed");
            }

        }
    }

    @Override
    public void onCreate() {
        mRecordingStatus = false;
        mServiceCamera = CameraRecorder.mCamera;
//		SurfaceView mSurfaceView = CameraRecorder.mSurfaceView;
        mSurfaceHolder = CameraRecorder.mSurfaceHolder;

        mListener = new SensorListener();
        mExecService = Executors.newFixedThreadPool(1);
        mValArray = new float[3][3];

        // Sensors
        mSenseManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccel = mSenseManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGyro = mSenseManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mMag = mSenseManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        // Alert tone
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        r = RingtoneManager.getRingtone(getApplicationContext(), notification);

        // Headset control
        ComponentName receiver = new ComponentName(getPackageName(), MediaButtonIntentReceiver.class.getName());
        mMediaSession = new MediaSessionCompat(this, "RecorderService", receiver, null);
        mMediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mMediaSession.setPlaybackState(new PlaybackStateCompat.Builder()
                .setState(PlaybackStateCompat.STATE_PAUSED, 0, 0)
                .setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE | PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_STOP)
                .build());
        mMediaSession.setCallback(mMediaSessionCallback);
        mMediaSession.setActive(true);
        IntentFilter filter = new IntentFilter();
        filter.setPriority(Integer.MAX_VALUE);
        filter.addAction(Intent.ACTION_MEDIA_BUTTON);
        MediaButtonIntentReceiver mMediaButtonIntentReceiver = new MediaButtonIntentReceiver();
        registerReceiver(mMediaButtonIntentReceiver, filter);

        super.onCreate();


    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (mMediaSession.getController().getPlaybackState().getState() == PlaybackStateCompat.STATE_PLAYING) {
            mMediaSession.setPlaybackState(new PlaybackStateCompat.Builder()
                    .setState(PlaybackStateCompat.STATE_PAUSED, 0, 0.0f)
                    .setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE).build());
        } else {
            mMediaSession.setPlaybackState(new PlaybackStateCompat.Builder()
                    .setState(PlaybackStateCompat.STATE_PLAYING, 0, 1.0f)
                    .setActions(PlaybackStateCompat.ACTION_PLAY_PAUSE).build());
        }


        return START_STICKY;
    }

    private final MediaSessionCompat.Callback mMediaSessionCallback
            = new MediaSessionCompat.Callback() {

        @Override
        public boolean onMediaButtonEvent(Intent mediaButtonEvent) {
            final String intentAction = mediaButtonEvent.getAction();
            if (Intent.ACTION_MEDIA_BUTTON.equals(intentAction)) {
                final KeyEvent event = mediaButtonEvent.getParcelableExtra(
                        Intent.EXTRA_KEY_EVENT);
                if (event == null) {
                    return super.onMediaButtonEvent(mediaButtonEvent);
                }
                final int action = event.getAction();
                if (event.getRepeatCount() == 0 && action == KeyEvent.ACTION_DOWN) {
                    Log.d(TAG, "Pressed headset key");

                    if (!mRecordingStatus)
                        startRecording();
                    else
                        stopRecording();

                }
            }
            return super.onMediaButtonEvent(mediaButtonEvent);
        }
    };

    @Override
    public void onDestroy() {
        if (mRecordingStatus)
            stopRecording();
        super.onDestroy();

    }

//    public void recordTimer() {
//        Handler t = new Handler();
//        int mRecordDuration = 15 * 1000;
//        t.postDelayed(stop, mRecordDuration + 1000);
//    }

    public boolean startRecording() {
        if (!mRecordingStatus) {


            mPath = getDataFilePath(getApplicationContext());
            mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            Intent bIntent = new Intent(RecorderService.this, CameraRecorder.class);
            PendingIntent pbIntent = PendingIntent.getActivity(RecorderService.this, 0, bIntent, 0);
            Notification.Builder bBuilder =
                    new Notification.Builder(RecorderService.this)
                            .setSmallIcon(R.drawable.icon)
                            .setContentTitle("CameraSensorRecorder")
                            .setContentText("Tap to return to app and stop")
                            .setSubText("Recording data...")
                            .setAutoCancel(true)
                            .setOngoing(true)
                            .setContentIntent(pbIntent);
//			startForeground(mNotificationId,bBuilder.build());
            mNotificationManager.notify(mNotificationId, bBuilder.build());
            try {
                Toast.makeText(getBaseContext(), "Recording Started", Toast.LENGTH_LONG).show();
                mServiceCamera = Camera.open();

                Camera.Parameters p = mServiceCamera.getParameters();

                final List<Camera.Size> listPreviewSize = p.getSupportedPreviewSizes();
//                for (Camera.Size size : listPreviewSize) {
//                    Log.d(TAG, String.format("Supported Preview Size (%d, %d)", size.width, size.height));
//                }

                Camera.Size previewSize = listPreviewSize.get(0);
                p.setPreviewSize(previewSize.width, previewSize.height);
                p.set("orientation", "portrait");

//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//                    int hasCameraPermission = checkSelfPermission(Manifest.permission.CAMERA);
//                    int hasMicPermission = checkSelfPermission(Manifest.permission.RECORD_AUDIO);
//                    int hasStoragePermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
//                }

                Camera.CameraInfo camInfo = new Camera.CameraInfo();
                Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, camInfo);

//                int cameraRotationOffset = camInfo.orientation;
//                int displayRotation = (cameraRotationOffset - 90 + 360) % 360;
//                int rotate = (360 + cameraRotationOffset - 90) % 360;
                p.setRotation(90);
                try {
                    mServiceCamera.stopPreview();
                    mServiceCamera.setPreviewDisplay(mSurfaceHolder);
                    mServiceCamera.setParameters(p);
                    mServiceCamera.setDisplayOrientation(0);
                    mServiceCamera.startPreview();

                } catch (IOException e) {
                    Log.e(TAG, e.getMessage());
                }


                mServiceCamera.unlock();
                mMediaRecorder = new MediaRecorder();
                mMediaRecorder.setCamera(mServiceCamera);
                mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
//                mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
//                mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
//                mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
//                mMediaRecorder.setVideoEncodingBitRate(2500);
                mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
                mMediaRecorder.setVideoFrameRate(30); // 10 Hz
                mMediaRecorder.setVideoSize(1280, 720);
                mMediaRecorder.setOutputFile(mPath + ".mp4");
                mMediaRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());

                mMediaRecorder.setOrientationHint(90);
				mMediaRecorder.setMaxDuration(mRecordDuration); // 15 seconds
                mMediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
                    @Override
                    public void onInfo(MediaRecorder mediaRecorder, int i, int i1) {
                        // Stops recording when max duration is reached
                        if (i == MEDIA_RECORDER_INFO_MAX_DURATION_REACHED){
                            Log.i(TAG,"Max duration reached");
                            stopRecording();
                        }

                    }
                });
                mMediaRecorder.prepare();
                mMediaRecorder.start();

                try {
                    mSensorFile = new CSVWriter(new FileWriter(mPath + ".csv"), ',', CSVWriter.NO_QUOTE_CHARACTER);
                } catch (IOException e) {
                    Log.d(TAG, "Can't write sensor file!");
                }
                mRecordingStatus = true;

                // Register sensor listeners with sampling frequency for gaming
                mSenseManager.registerListener(mListener, mAccel, SensorManager.SENSOR_DELAY_GAME);
                mSenseManager.registerListener(mListener, mGyro, SensorManager.SENSOR_DELAY_GAME);
                mSenseManager.registerListener(mListener, mMag, SensorManager.SENSOR_DELAY_GAME);
                mCsvHandler.postDelayed(csvTask, mSavePeriod);
                Log.d("RecorderService", "Recording in the background.");
                return true;

            } catch (IllegalStateException e) {
                Log.d(TAG, e.getMessage());
                mMediaRecorder.release();
//                e.printStackTrace();
                return false;

            } catch (IOException e) {
                Log.d(TAG, e.getMessage());
                mMediaRecorder.release();
//                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    private TimerTask csvTask = new TimerTask() {
        @Override
        public void run() {

                for (float[] arr : mValArray) {
                    for (float a : arr) {
                        mValBuf.append(a);
                        mValBuf.append(',');
                    }
                }
                String sb = mValBuf.toString();
                Log.d(TAG, "CSV data written: " + sb);
                String[] sb_split = sb.split(",");
                mSensorFile.writeNext(sb_split);
                mValBuf = new StringBuffer();

                mCsvHandler.postDelayed(this, mSavePeriod);

        }
    };
    private TimerTask stop = new TimerTask() {
        @Override
        public void run() {
            stopRecording();
        }
    };

    public void stopRecording() {
        mNotificationManager.cancel(mNotificationId);
        try {
            mExecService.shutdown(); // Disable new tasks from being submitted
            mExecService.shutdownNow();
        } catch (Exception e) {
            // (Re-)Cancel if current thread also interrupted
            mExecService.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
            Log.d(TAG,e.getMessage());
        }


//        try {
//			mExecService.awaitTermination(60, TimeUnit.SECONDS);
//
//			// Wait a while for existing tasks to terminate
//
//			if (!mExecService.awaitTermination(60, TimeUnit.SECONDS)) {
//				mExecService.shutdownNow(); // Cancel currently executing tasks
//				// Wait a while for tasks to respond to being cancelled
//				if (!mExecService.awaitTermination(60, TimeUnit.SECONDS))
//					System.err.println("Pool did not terminate");
//			}
//		} catch (InterruptedException ie) {
//			// (Re-)Cancel if current thread also interrupted
//			mExecService.shutdownNow();
//			// Preserve interrupt status
//			Thread.currentThread().interrupt();
//		}
        try {
            mSenseManager.unregisterListener(mListener);
        } catch (Exception e) {
            Log.d(TAG,e.getMessage());
        }

		try {
			mServiceCamera.reconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

        mMediaRecorder.stop();
        mMediaRecorder.reset();

        try {
            mServiceCamera.stopPreview();
            mServiceCamera.release();
        } catch (Exception e) {
            Log.d(TAG,e.getMessage());
        }
        mMediaRecorder.release();


//        mServiceCamera = null;
        try {
            mCsvHandler.removeCallbacks(csvTask);
            mSensorFile.flush();
            mSensorFile.close();
            Log.d(TAG,"Saved recording to: " + mPath + ".csv");
        } catch (IOException e) {
            Log.d(TAG, "Sensor file not closed.");
        } catch (Exception e) {
            Log.d(TAG,e.getMessage());
        }
        mRecordingStatus = false;
        Toast.makeText(getBaseContext(), "Recording Stopped", Toast.LENGTH_SHORT).show();
        Log.d("RecorderService", "Saved recording to: " + mPath + ".mp4 & .csv");
        r.play();


    }

    private String getDataFilePath(Context context) {
        String path;

        try {
            path = context.getExternalFilesDir(null).getAbsolutePath() + "/"
                    + System.currentTimeMillis();
        } catch (NullPointerException e) {
            Log.d(context.getPackageName(), "File can't be written");
            path = "";
        }
        return path;
    }


}
