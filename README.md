# CameraSensorRecorder
This app records camera and sensor data simultaneously.

Sensors included: 
* Accelerometer
* Gyroscope
* Magnetometer/Compass

Changelog:

- Now using service bindings
